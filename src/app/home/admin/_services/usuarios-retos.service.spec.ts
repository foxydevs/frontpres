import { TestBed, inject } from '@angular/core/testing';

import { UsuariosRetosService } from './usuarios-retos.service';

describe('UsuariosRetosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsuariosRetosService]
    });
  });

  it('should be created', inject([UsuariosRetosService], (service: UsuariosRetosService) => {
    expect(service).toBeTruthy();
  }));
});
