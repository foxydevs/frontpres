import { TestBed, inject } from '@angular/core/testing';

import { PresentacionesService } from './presentaciones.service';

describe('PresentacionesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PresentacionesService]
    });
  });

  it('should be created', inject([PresentacionesService], (service: PresentacionesService) => {
    expect(service).toBeTruthy();
  }));
});
