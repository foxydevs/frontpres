import { TestBed, inject } from '@angular/core/testing';

import { RetosService } from './retos.service';

describe('RetosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RetosService]
    });
  });

  it('should be created', inject([RetosService], (service: RetosService) => {
    expect(service).toBeTruthy();
  }));
});
