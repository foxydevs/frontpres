import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { DataTableModule } from "angular2-datatable";


import { ClienteRoutingModule } from './cliente.routing';
import { ClienteComponent } from './cliente.component';
import { LoaderComponent } from './loader/loader.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';

import { EventsService } from './../admin/_services/events.service';
import { CategorysService } from './../admin/_services/categorys.service';
import { ProductsService } from './../admin/_services/products.service';
import { OrdersService } from './../admin/_services/orders.service';
import { UsuariosService } from "./../admin/_services/usuarios.service";

import { SimpleNotificationsModule } from 'angular2-notifications';
import { ChartsModule } from 'ng2-charts';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2DragDropModule } from 'ng2-drag-drop';
import { LoadersCssModule } from 'angular2-loaders-css';
import { Ng2MapModule} from 'ng2-map';

import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { ProductosComponent } from './productos/productos.component';
import { EventosComponent } from './eventos/eventos.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { ReciboComponent } from './recibo/recibo.component';
import { NoticiasComponent } from './noticias/noticias.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2SearchPipeModule,
    Ng2DragDropModule.forRoot(),
    Ng2MapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyCdJAwErIy3KmcE_EfHACIvL0Nl1RjhcUo'}),
    LoadersCssModule,
    ClienteRoutingModule
  ],
  declarations: [
    ClienteComponent,
    LoaderComponent,
    DashboardComponent,
    ProfileComponent,
    ConfiguracionComponent,
    EventosComponent,
    ProductosComponent,
    CategoriasComponent,
    PedidosComponent,
    ReciboComponent,
    NoticiasComponent
  ],
  providers: [
    UsuariosService,
    EventsService,
    CategorysService,
    ProductsService,
    OrdersService
  ]
})
export class ClienteModule { }
